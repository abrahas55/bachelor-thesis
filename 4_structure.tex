\section{Strukturanalyse und Schutzbedarf}

Zunächst muss eine Strukturanalyse durchgeführt werden. Dabei werden in einem 
top-bottom Verfahren die Geschäftsprozesse definiert und die dazugehörigen 
Anwendungen aufgeschlüsselt. Schließlich werden die IT-Systeme benannt, auf 
denen die Anwendungen laufen bzw.\@ auf die sie zugreifen. 

Im Grundschutz-Vorgehen ist es vorgesehen, die erfassten Objekte mit einem 
Kürzel zu identifizieren. Dabei ist grundsätzlich jede Art Kürzel möglich und 
es sollte vor allem für die Durchführenden wiedererkennbar sein. In dieser Arbeit
nutze ich \textit{Sx} für Systeme und \textit{Ax} bzw. \textit{XA} für die 
Anwendungen.\footnote{Die konkrete Benennung der Zielobjekte ist im Grundschutz 
nicht festgelegt und kann beliebig gewählt werden. Man kann sich an den bestehenden 
Beispielen bei der Namensgebung orientieren oder freie, sinnvolle Identifier definieren.}

Üblicherweise werden in der Analyse auch Umgebungsparameter, wie der Zugang
zum Gelände, aufgenommen. Wie bereits erwähnt handelt es sich bei einem Cluster,
insbesondere im Kontext einer Forschungsinstitution, nicht um ein eigenständiges
System. Es gibt immer einen größeren Kontext, z.\@B.\@ in unserem Fall ist
Curta Teil des Netzes der FUB-IT, die wiederum selbst Teil der Freien Universität
Berlin ist. Somit muss für eine vollständige Betrachtung immer der Kontext
gesehen werden. Dies ist zwar auf der einen Seite mit einem höheren Aufwand für
die Durchführung verbunden, da nunmehr weitere Positionen am Prozess beteiligt
sind und mehr Kommunikation nötig ist. Jedoch können so viele Schritte, die bereits außerhalb des Kontextes
\enquote{Cluster} durchgeführt wurden, für uns übernommen werden. 

Explizit werden jene Schritte übersprungen, von denen auszugehen ist, dass 
sie bereits als Teil des Sicherheitskonzeptes einer höheren Instanz (FUB-IT, 
Freie Universität Berlin, DFG, etc.\@) durchgeführt wurden. Ferner führe ich hier 
nur eine Kern-Absicherung durch, in der ich mich auf den Aspekt der Forschung fokussiere. 

\subsection{IT-Strukturanalyse}

In einer obersten, abstrakten Schicht befassen wir uns zunächst mit den
relevanten Geschäftsprozessen. Dies sind jene Prozesse, deren Durchführung zu
den Geschäftszielen führen. Das BSI sieht dabei eine Strukturierung in Kern- und
unterstützende Prozesse vor, diese werden in dieser Arbeit aber zusammengefasst.
Die Unterscheidung basiert darauf, ob Prozesse direkt zum Erreichen eines
Geschäftsziels dienen oder unterstützen. Diese Differenzierung ist in einer
Gesamtbetrachtung durchaus sinnvoll, für den Zweck dieser Arbeit aber zu
feingliedrig. 

Bevor wir aber festlegen können, welche Geschäftsprozesse relevant sind, sollten
wir uns klar werden, welche Geschäftsziele ein Forschungscluster überhaupt hat.
Natürlich ist ein offensichtliches Ziel (und dazu passender Prozess) die
Forschungsunterstützung durch die Clusterressourcen (also bspw.\@ das Simulieren
von beliebig vielen Massepunkten in einem physikalischen Raum zur Beobachtung der
Bewegungen und gegenseitigem Einfluss\footnote{siehe auch: n-body 
problem~\cite{n-body}}. Aber damit das Cluster funktioniert, muss auch die
Verwaltung und Administration stattfinden (Dies wäre dann ein unterstützender
Prozess). Sollte die Forschung das Cluster aber nicht (nur) als Ressource,
sondern als Forschungsgegenstand sehen, ist die Verwaltung des Clusters
gleichzeitig auch Forschungsgegenstand. Man sieht schnell, dass das Definieren
von Geschäftsprozessen immer sehr spezifisch mit der Institution verknüpft ist.
Umso wichtiger ist dieser Schritt für den Gesamterfolg. Auf der anderen Seite
ist es durchaus möglich und üblich, auch noch im Nachhinein Prozesse zu
ergänzen, die beim initialen Assessment nicht betrachtet wurden. 

\subsubsection{Geschäftsprozesse}

Für unser einfaches Beispiel ist das einzige Geschäftsziel die erfolgreiche 
Forschung mithilfe des Clusters (GP1). Als unterstützender Prozess könnte auch die 
Administration des Clusters (GP2) miteinbezogen werden. Ohne diese Wartungs- und 
Optimierungsarbeiten wäre das Betreiben des Clusters nicht möglich. Dieser 
unterstützende Prozess wird jedoch nur marginal betrachtet, da nicht zu erwarten
ist, dass sich die Maßnahmen und Anforderungen dafür von der klassischen Büro-IT
unterscheiden werden.

\subsubsection{Anwendungen}

Relevant für die Administration sollte es sein, ein solides Verständnis der 
eingesetzten Anwendungen zu besitzen und darauf basierend eine Abschätzung 
für den Schutzbedarf treffen zu können. In unserem Fall reicht eine grobe 
Kategorisierung aus, denn ich betrachte die eingesetzte Software in abstrakten 
Klassen. Dies hat den Vorteil, dass man nicht an die konkrete Software gebunden 
ist. Aufgrund der Sicht auf den Cluster wäre auch garnicht klar, welche Software
dort gerade betrieben wird und für wie lange. Je nach Art des Clusters kann es 
natürlich auch vorkommen, dass Software über viele Monate oder Jahre hinweg 
genutzt wird. Dann kann es aus einer Ressourcenperspektive sinnvoll 
sein, diese Anwendungen in die Liste explizit mit aufzunehmen und einzeln zu 
betrachten. Im optimalen Fall werden die Anwendungen vor dem Einsatz stets
in das ISMS aufgenommen und überprüft, sodass jede Anwendung angemessen betrachtet
wird. Dies setzt aber voraus, dass ein entsprechendes ISMS (z.B. im Rahmen einer
IT-Grundschutz Durchführung) etabliert ist und dass die Ressourcen für die 
kontinuierliche Arbeit eingeplant sind. 

Wir teilen die Anwendungen in Fachanwendungen (FA) und Betriebsanwendungen (BA)
auf. Letztere inkludieren alle Anwendungen, die für den Betrieb des Clusters
relevant sind. Dazu zählen das Betriebssystem sowie etwaige administrativ
genutzte Tools. Unter die Fachanwendungen fallen alle Anwendungen der 
Forschungsgruppen. Diese können hier nur begrenzt betrachtet werden, da sie 
immer davon abhängig sind, welche Forschung gerade betrieben wird. Ich werde 
abstrakt zwei Gruppen von Fachanwendungen mit aufnehmen, einmal für solche 
mit normalem und jene mit hohem bzw.\@ sehr hohem Schutzbedarf. Gemessen an
dem Szenario und da eine Kern-Absicherung angestrebt wird, werde ich mich in
der Arbeit auch auf die Fachanwendungen mit höherem Schutzbedarf fokussieren. 

\subsubsection{IT-Systeme}

Anwendungen laufen auf \textit{IT-Systemen}. Diese müssen natürlich ebenfalls 
betrachtet werden. Neben den hier aufgeführten Systemen sind noch einige weitere 
im Gesamtkomplex beteiligt, darunter fällt z.\@ B.\@ das Speichersystem. Dieses 
wird nicht nur im Rahmen des Clusters genutzt, sondern durch die übergeordnete 
Institution FUB-IT verwaltet und bereitgestellt. An dieser Stelle sollten 
Verantwortliche bei der Umsetzung des Grundschutzes das entsprechende 
Sicherheitskonzept der FUB-IT einsehen und die dort getätigten Annahmen 
verifizieren und gegebenenfalls so modifizieren, dass sie dem betrachteten 
Verbund (also dem HPC-Cluster) und dem dafür angebrachten Schutzbedarf gerecht 
werden. Im Rahmen dieser Arbeit werde ich davon ausgehen, dass diese 
nicht-betrachteten Systeme angemessen im Sicherheitskonzept der FUB-IT mitbedacht 
wurden. 

Als Grundbestandteile eines Clusters können wir nun darüber hinaus einige
Bestandteile identifizieren, die für unsere Strukturanalyse relevant sind. Der 
offensichtliche Kandidat sind die Knoten, wobei wir uns hier zunächst auf die 
Login-Knoten (\textit{S0}) beschränken, die für den Zugang auf das Cluster
verwendet werden. Darüber loggen sich sowohl Nutzende als auch Administrierende
ein, um geplante Tätigkeiten zu initiieren oder den Status eines aktuellen Jobs 
einzusehen. Der Zugang zu den \textit{Login-Knoten} (und damit zum Cluster selbst) 
ist nur über das Uninetzwerk (bzw.\@ eine VPN-Verbindung dahin) möglich. Diese 
Trennung ist grundsätzlich ein Standardverfahren und wird auch in den gängigen 
Publikationen vorausgesetzt. Darüber hinaus sind auch die Rechenknoten \textit{(S0\_a)}
zu beachten, die zwar von der reinen Konfiguration den Login-Knoten ähneln, jedoch 
nicht von außen erreichbar sind. Nur der Job-Scheduler kann auf diese zugreifen und die 
entsprechenden Rechenjobs verteilen. 

Auch die Netzwerkkomponenten (\textit{S1}) wie Switches und Router, insbesondere 
mit Anbindung an externe Netze (wie das Internet), müssen hier Beachtung finden. 
Aufgrund des Netzplanes kennen wir zwei Arten von Switches, diese können wir 
allerdings als eine Gruppe zusammenführen, da die Unterscheidung nur der Topologie 
dient, darüber hinaus aber keine Sicherheitsfeatures anzunehmen sind.

\subsubsection{Infrastruktur}

An dieser Stelle betrachtet man im Grundschutz üblicherweise die Infrastruktur des
Informationsverbundes. Darunter fallen natürlich Räumlichkeiten, aber auch weitere 
Geräte, die zunächst nicht direkt mit der Informationssicherheit verbunden werden, 
wie Klimaanlagen oder auch Kaffeemaschinen. Auch hier sollte am Ende ein 
guter Überblick herrschen, welche Geräte wo betrieben werden, um etwaige Auswirkungen
auf den Gesamtverbund erkennen zu können. 

Wir bereits angesprochen betrachte ich den Cluster als ein Teilsystem des gesamten
Komplexes der \textit{Freien Universität Berlin} bzw.\@ deren \textit{FUB-IT}. Somit 
sind auch die Räumlichkeiten und entsprechenden Infrastrukturgeräte Teil deren 
Sicherheitskonzepte und können bei unserer Betrachtung außen vor gelassen werden. 
In einer vollen Betrachtung sollten die angewandten Maßnahmen nochmal auf den 
Bedarf für den Clusterbetrieb angepasst werden, mindestens aber Erwähnung im 
Sicherheitskonzept finden. 

\subsection{Schutzbedarfsanalyse}

Nach der entsprechenden Kategorisierung werden nun zunächst den erfassten 
Geschäftsprozessen ein Schutzbedarf zugeordnet. Danach den jeweils 
beteiligten Anwendungen und den IT-Systemen, auf denen diese Anwendungen laufen. 
Diese Reihenfolge erlaubt die \textit{Vererbung} des Schutzbedarfes nach unten. 
Hat z.\@ B.\@ eine Anwendung einen hohen Schutzbedarf, so ergibt sich auch 
für die Systeme, die für diese Anwendung benötigt werden ein entsprechender 
Schutzbedarf. Es ist hierbei wichtig zu betonen, dass ein höherer Schutzbedarf 
nicht allzu freizügig vergeben werden sollte. Oftmals betrachtet eine 
anwendende Person das System als höchst kritisch, wobei die Auswirkung auf 
die Gesamtinstitution eher marginal ist. Relevant für den Schutzbedarf sollte 
stets sein, welche Effekte der Ausfall eines Prozesses oder Systems für die 
Institution hat. 

Zunächst muss für die Schutzbedarfsanalyse eine angemessene Betrachtung der
Schutzbedarfseigenschaften vorgenommen werden. Diese werden in der üblicherweise
verwendeten Konstellation \textit{Verfügbarkeit, Integrität, Vertraulichkeit}
auch hier Anwendung finden. Das BSI beschreibt im Standard~\cite{bsi-200-2} 
eine allgemeine, qualitative Beschreibung~\ref{schutzbedarf_definition}.  

Diese Bewertungen sind für alle Grundwerte anzuwenden und nach Möglichkeit
entsprechend quantitative Aussagen zu treffen. So könnte bei der Verfügbarkeit
eine maximale Ausfalltoleranz von 10 Ausfällen pro Jahr einem normalen
Schutzbedarf entsprechen. Da diese quantitative Einschätzung aber nur 
von der betroffenen Institution bestimmt werden kann (und dies auch explizit
so vom BSI gefordert wird), verbleibe ich bei der allgemeinen Definition 
und gebe bei Bedarf beispielhafte Auswirkungen an. 

Es ist allerdings schwer, für solche Kriterien eine allgemeine Aussage treffen zu können. 
Gleiche Prozesse können in unterschiedlichen Institutionen auch unterschiedliche
Schutzbedarfe haben. Ich werde hier einige sinnvolle Annahmen treffen, aus denen
sich dann die weiteren Schritte ableiten werden. In der Umsetzung des Grundschutzes
kann man im Hinterkopf behalten, dass dieser auch eine regelmäßige Revision vorsieht, 
zu denen sich auch der Schutzbedarf grundsätzlich ändern kann. Dies mag auf den 
ersten Blick nicht nachvollziehbar wirken, aber man stelle sich ein Unternehmen vor, 
dass zunächst ohne Internetkommunikation auskommt und einen Zugang nur als Bonus 
für die Mitarbeitenden im Büro stellt. Mit dem Zuwachs der Relevanz der Internetpräsenz 
und dem zunehmenden, geschäftlichen Internetverkehr steigt auch die Kritikalität 
des Internetzugangs für das Unternehmen. Dieser Vorgang ist normal und da sich 
gerade der technische Bereich immer weiterentwickelt müssen Institutionen sich auch 
auf die neuen Begebenheiten anpassen können. 

Ich betrachte für den Schutzbedarf hier nur den Prozess der Forschung (GP1), da
wie oben begründet eine allgemeine, breite Betrachtung nicht viel Mehrwert erzeugt. 
Ich nehme an, dass einige Forschungsanwendungen einen hohen Schutzbedarf haben. 
Dieser ergibt sich aus dem Maximalprinzip der einzelnen Grundwerte 
\textit{Verfügbarkeit, Integrität und Vertraulichkeit}, die im folgenden erläutert 
werden. Daraus ergeben sich später relevante Maßnahmen und Risiken.

Ich nehme an, dass die Forschungsaufgaben keine personenbezogenen oder geheimen 
Daten verarbeiten, jedoch die Intergrität der Eingangsdaten und Ergebnisse 
unbedingt gewahrt sein muss. Ferner benötigt die Forschungsanwendung eine hohe
Verfügbarkeit, da die Ergebnisse für die politischen Entscheidungen der nächsten
Monate relevant seien. Es folgt also für den Schutzbedarf der Grundwerte folgendes
Tripel \textit{(Verfügbarkeit: hoch, Integrität: hoch, Vertraulichkeit: normal)}.

Nun betrachten wir die Anwendungen, die für diesen Geschäftsprozess nötig sind. 
Aufgrund der sehr groben Kategorisierung der Prozesse folgt aus der Vererbung 
ein hoher Schutzbedarf für jegliche Forschung. Sollte auch nicht-kritische Forschung
auf dem Cluster betrachtet werden, muss formal gesehen eine Differenzierung 
bereits im Geschäftsprozess stattfinden. Ebenfalls können Anwendungen oder IT-Systeme
auch für mehrere Geschäftsprozesse relevant sein. Aufgrund dieser in der Praxis
oft sehr komplexer Zusammenhänge ist ein strukturiertes Vorgehen umso wichtiger. 

\subsection{Modellierung}

Ich orientiere mich bei den Bausteinen am Grundschutzkompendium in der Edition 2023. 
Bausteine, die dort nicht mehr aufgeführt sind sehe ich als veraltet an. Ein Baustein
ist eine Sammlung an Anforderungen und Maßnahmen zur Sicherung von Zielobjekten. 
Dabei können durchaus mehrere Bausteine auf ein Zielobjekt abgebildet werden 
(Für einen Webserver unter Linux sind die Bausteine \textit{APP.3.2 Webserver, 
SYS.1.1 Allgemeiner Server} und \textit{SYS.1.3 Server unter Linux und Unix} anzuwenden). 

Neben diesen sogennanten systemorientierten Bausteinen (da sie sich auf konkrete 
technische Systeme beziehen) gibt es noch die prozessorientierten Bausteine. Wie 
der Name schon impliziert, handelt es sich dabei um Anforderungen zu den Prozessen 
in der Organisation. Diese sind meist allgemein auf den Informationsverbund anzuwenden,
werden aber hier als bereits durchgeführt angesehen, da die Prozesse bereits in
der FUB-IT implementiert sein sollten. 

Im Grundschutz ist es vorgesehen, dass es Zielobjekte geben kann, die mit den 
bestehenden Bausteinen nicht abgebildet werden können. Für diesen Fall muss eine 
Risikoanalyse durchgeführt werden.~\cite{bsi-200-3} Diese wird im Anschluss an 
die Modellierung gemacht.

Grundsätzlich sind beim Einsatz des Grundschutzes nicht alle Bausteine nötig.
Die Wahl der anzuwendenden Bausteine ist abhängig von der Ausprägung des
Informationsverbundes und der eingesetzten Methoden und Technologien. Eine beispielhafte
Modellierung für das Cluster ist im Anhang in der Tabelle~\ref{modellierung_matrix} zu
finden. Für jeden Baustein wird dabei aufgeführt, ob er betrachtet wird und warum. 
Diese Tabelle dient eher der Übersicht und wird oft verbunden mit der Anzahl der 
Vorkommen der einzelnen Bausteine. Dies habe ich in diesem Fall weggelassen, da uns
dadurch kein wirklicher Mehrwert entsteht. Einige Bausteine, z.~B.~ \textit{ISMS.1}, 
sind so wichtige Bausteine, dass sie im Prinzip angewandt werden (können), unabhängig 
davon, wie der konkrete Informationsverbund aufgebaut ist. Diese habe ich zur einfachen 
Erkennung als \textit{MUSS} markiert. Gleichzeitig kann bei diesen davon ausgegangen werden, 
dass die FUB-IT sie in ihrem Sicherheitskonzept mitbedacht hat. Den aufmerksamen Lesenden 
wird nicht entgangen sein, dass es sich dabei fast ausschließlich um Prozessbausteine handelt. 

Spannend sind die Bausteine, die über die \enquote{Basis-IT} hinaus abdecken. 
Dazu betrachten wir die für uns relevante Anwendung \textit{FA1} und die dazugehörigen
IT-Systeme, die Knoten und Switches des Cluster. Die Tabelle~\ref{modellierung_GP1} 
zeigt die angewandten Bausteine für jedes Zielobjekt mit Relevanz für den 
Geschäftsprozess GP1 (Forschung). Hierbei werden jedem Zielobjekt die passenden
Bausteine aus dem Kompendium zugeordnet. Die in den Bausteinen beschriebenen 
Anforderungen sind also für diese Zielobjekte umzusetzen.

Im Rahmen dieses Prozesses ergab sich während der Ausarbeitung eine weitere 
Anwendung, der Scheduler \textit{Slurm}, die vorher zunächst nicht relevant schien. 
Nachdem man sich auf einem Login-Knoten mittels SSH eingeloggt hat, benutzt man 
diesen um seine Simulation zu starten. Somit ist diese Anwendung auch relevant für
den Prozess der Forschung und muss damit auch in der Kern-Absicherung betrachtet werden. 
Gleichzeitig fällt allerdings auf, dass nur der Systembaustein \textit{APP.6 Allgemeine Software} 
passend ist. Zwar kann es einzelne Anforderungen aus anderen Bausteinen geben, die auch 
für diese Anwendung förderlich wären, jedoch passen die Beschreibungen der anderen 
Bausteine nicht gut genug auf unsere sehr spezifische Anwendung. 

\subsubsection{Zusammenfassung der bisherigen Schritte}

Bis hierhin habe ich die vorhandene Struktur analysiert und ihren Schutzbedarf festgelegt. 
Daraufhin wurden den Zielobjekten passende Bausteine zugewiesen, wobei aufgrund der 
Spezialisierung des Grundschutzes auf Büro-IT hier nur wenig Bausteine passten. 
Somit sind zwar einige Maßnahmen eruierbar, jedoch fehlt dennoch ein genügender 
Überblick über die Sicherheitslage der Systeme. Neben dem Ziel, Maßnahmen für bestimmte 
Systeme zu ermitteln enthalten die Bausteine auch eine implizite Risikoeinschätzung. 
Diese ist also ebenfalls nicht gegeben und somit kann keine ausreichende Aussage über den
Schutz der Forschungsanwendung und seiner beteiligten IT-Systeme gemacht werden.
Kann ein Zielobjekt nicht ausreichend durch Bausteine abgebildet werden, müssen 
diese Gefährdungen betrachtet werden. Auch dies geschieht in der Risikoanalyse, die im 
\textit{Standard 200-3} beschrieben ist. 

Im nächsten Schritt würden zunächst die ermittelten Anforderungen aus den Bausteinen umgesetzt. 
Dazu würden jeweils Arbeitspakete erstellt und an die verantwortlichen Instanzen 
übermittelt werden. Dabei kann durchaus von den Maßgaben im Kompendium abgewichen werden, 
wenn eine geeignete Abschätzung und Begründung vorliegt. So kann die Anforderung \textit{OPS.1.1.4.A1
Erstellung eines Konzepts für den Schutz vor Schadprogrammen} als Ergebnis haben, 
das bestimmte Zielobjekte nicht durch ein Virenschutzprogramm geschützt werden müssen. 
Dies würde zunächst der Anforderung \textit{OPS.1.1.4.A3} widersprechen, jedoch kann 
das Konzept beinhalten, dass ein Virenschutzprogramm den Aufgaben des Zielobjektes 
widerspricht, weil dort z.B.\@ Schadsoftware getestet wird. Insbesondere im HPC-Bereich 
ist hier die Frage nach der Performanz zu stellen und etwaige Sicherheitsvorkehrungen 
mit deren Auswirkungen immer abzuschätzen. Werden Anforderungen nicht umgesetzt, muss 
dies im folgenden Schritt, der Risikoanalyse, begründet werden.

Die Kern-Absicherung sieht vor der Risikoanalyse noch den IT-Grundschutz-Check vor. Dieser 
Schritt vergleicht die ermittelten Sicherheitsanforderungen aus den vorherigen Teilen mit
der realen Situation, indem Interviews mit den verantwortlichen Personen durchgeführt werden. 
Diesen Schritt kann ich nicht zeitlich (und logistisch) unterbringen, somit nehme ich an, 
dass die beschriebenen Anforderungen entweder wie im Grundschutz beschrieben umgesetzt sind 
oder entsprechende Ausnahmen begründet sind. 
