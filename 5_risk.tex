\section{Risikoanalyse}

Die Risikoanalyse nimmt im Grundschutz eine wichtige Rolle ein. Da die
Modellierung offensichtlich nur jene Systeme abdecken kann, für die auch ein
Baustein konzipiert wurde und diese Bausteine zwangsläufig nicht jeden
spezifischen Fall komplett abbilden können, muss für diese Komponenten eine
weitere Bewertung und eventuelle Sicherung durch Maßnahmen erfolgen. Diese Rolle 
wird im Grundschutz durch die Risikoanalyse eingenommen. Ebenfalls werden die 
Komponenten mit höherem Schutzbedarf dadurch noch einmal betrachtet. Dies ist 
ganz im Sinne der Abwägung zwischen Eintrittswahrscheinlichkeit und potentiellem Schaden. 
Dazu definiert das BSI einige elementare Gefährdungen, die bereits in der 
Modellierung durch die entsprechenden Anforderungen einbezogen werden. Gibt es
nun im Informationsverbund Teile, die durch die Bausteine nicht (vollständig) 
abgebildet werden können, müssen diese elementaren Gefährdungen entsprechend
nochmal im Einzelnen für das Objekt betrachtet werden. Eine Auflistung der 
elementaren Gefährdungen ist in der Tabelle~\ref{gefahren} dargestellt. Jede
Gefährdung hat dabei eine ID und zugeordnete Grundwerte (Confidentiality, 
Integrity und Availability), auf die sich die Gefährdung auswirkt. Beispielhaft
sorgt die Gefährdung \textit{G0.1 Feuer} dafür, dass das Objekt nicht mehr verfügbar
ist, jedoch findet dadurch keine Änderung oder Weiterleitung der Daten statt. 

In der Modellierung betrachtete ich die Forschungsanwendungen mit hohem Schutzbedarf 
und stellte fest, dass die angewandten Bausteine nicht ausreichen, um eine geeignete
Aussage zur Gefährdung zu treffen. Daher werden die beteiligten Anwendungen und 
IT-Systeme in der Risikoanalyse miteinbezogen. Insbesondere die Anwendung
\textit{FA1 Forschungsanwendung mit höherem Schutzbedarf} ist hier zu bemerken, 
da sie zusätzlich zu ihrem festgestellten höherem Schutzbedarf auch nicht ausreichend
abgedeckt ist. Somit ist diese Art von Zielobjekt umso eher zu betrachten. Die 
Frage, ob eine Risikoanalyse durchzuführen ist, verbleibt allerdings bei der 
Institution. Je nach Verfügbarkeit der Ressourcen und dem Einsatzzweck kann 
sich grundsätzlich auch dagegen entschieden werden. Dies ist in einem Sicherheitskonzept
stets zu begründen. 

Für diese Arbeit werde ich daher nur die Forschungsanwendung mit höherem Schutzbedarf 
betrachten. Damit sollte das Vorgehen und die Problematik ausreichend nachvollziehbar 
sein. 

Nach dem Grundschutz~\cite{bsi-200-3} sind zunächst die Gefährdungen zu
ermitteln. Dabei werden die in der Methodik~\cite{bsi-200-2} vorgestellten
elementaren Gefährdungen zugrunde gelegt und bei Bedarf um zusätzliche
Gefährdungen erweitert. Diese Gefährdungen müssen zu einem nennenswerten Schaden
führen können und im vorliegenden Anwendungsfall und Einsatzumfeld realistisch
sein. Dabei muss der Schutzbedarf des jeweiligen Zielobjektes ebenfalls
miteinbezogen und die Gefährdungen entsprechend ausgewählt werden. Für die 
betrachtete Anwendung ist dieser Schritt im Anhang in der Tabelle~\ref{gefahren_FA1} 
einzusehen. Die aufgelisteten Gefährdungen entsprechen den elementaren Gefährdungen,
die für das Zielobjekt möglich sind. 

Als Nächstes wird das Risiko eingestuft. Dazu bedarf es angemessener
Einschätzungen der Eintrittswahrscheinlichkeit und den erwarteten Auswirkungen
bzw.\@ der Schadenshöhe. Dies setzt natürlich ein gutes Verständnis des Systems
und der dazugehörigen Prozesse voraus. An dieser Stelle muss ich einige Annahmen 
treffen, die eventuell im realen Clustersystem \textit{Curta} anders zu bewerten 
sind, wodurch das grundsätzliche Vorgehen aber nicht weiter betroffen sein sollte. 

Da es sich bei FA1 um Software handelt, können all jene Gefährdungen, die nur 
direkten Einfluss auf (Hardware-)Infrastruktur haben, bereits als irrelevant bzw.\@
indirekt relevant eingestuft werden. So ist die Gefährdung \textit{G0.1 (Feuer)} 
natürlich in gewisser Weise relevant, da die Anwendung (vermutlich) nicht 
ordnungsgemäß funktioniert, wenn ein Feuer im Clustersystem ausbricht. Dennoch ergibt
sich aus der Betrachtung keine neue Erkenntnis für die Anwendung, wodurch
diese Gefährdung in der Risikoanalyse nicht weiter betrachtet werden muss. Eine
analoge Schlussfolgerung ergibt sich aus ähnlichen Gefährdungen (beispielsweise
\textit{G0.3, G0.4, G0.5, usw.\@}).  

Da der Baustein \textit{APP.6 (Allgemeine Software)} für dieses Zielobjekt 
angewandt wurde, ist die Gefährdung \textit{G0.20 (Informationen oder Produkte 
aus unzuverlässiger Quelle)} für die Herkunft (und Updates) der Software bereits 
einkalkuliert. Jedoch ist ebenfalls die Herkunft und die Überprüfung der Daten 
Teil dieser Gefährdung. Dementsprechend sind also Maßnahmen zur Wahrung der 
Integrität für das Zielobjekt \textit{FA1} zu prüfen. Ebenfalls ist die (recht 
allgemeine) Gefährdung \textit{G0.25 (Ausfall von Geräten oder Systemen)} relevant. 
Offensichtlich sollte also überprüft werden, ob Maßnahmen zur Sicherung der 
Verfügbarkeit getroffen werden müssen, falls das Cluster (oder Teile davon) ausfallen. 

Eine der beschriebenen elementaren Gefährdungen, \textit{G0.26 (Fehlfunktion von 
Geräten oder Systemen)}, hat als beigefügtes Beispiel eine 
wissenschaftliche~\footnote{diese explizite Erwähnung erschließt sich dem Autor nicht. 
Unabhängig des konkreten Anwendungsfeldes ist die beschriebene Problematik vorhanden.}
Anwendung, die aufgrund fehlender Kompatibilität falsche Ergebnisse in eine 
Datenbank übernimmt. Diese Problematik ist bereits durch die Basisanforderung 
\textit{APP.6.A1 (Planung des Software-Einsatzes)} abgedeckt. Innerhalb dieser 
Anforderung sollte eruiert werden, \enquote{wie die Software an weitere Anwendungen 
und IT-Systeme über welche Schnittstellen angebunden wird}~\cite{bsi-kompendium}.

Auf Basis dieser Risikoeinschätzung muss dann bewertet werden, wie mit den
Risiken umgegangen wird. Der Grundschutz sieht neben dem Akzeptieren von Risiken 
die Reduktion durch Maßnahmen oder das Transferieren vor. Letzteres bezieht sich auf 
das Teilen (oder Transferieren) des Risikos mit einer anderen Institution. Transfer 
heißt in unserem Fall eigentlich immer, dass die Gefährdung ebenfalls relevant für die 
übergeordnete Institution FUB-IT bzw. FUB ist. Das heißt natürlich nicht, dass die 
Gefährdung ignoriert werden sollte, sondern dass die Kommunikation dahin aufgenommen 
werden soll und ggfs.\@ gemeinsam Ressourcen investiert werden. Reduktion beschreibt die 
Notwendigkeit einer oder mehrerer Maßnahmen. Diese können auch für mehrere Szenarien 
eine Verbesserung bewirken. Diese Maßnahmen sollten zusätzlich(!) zu den während der 
Modellierung festgestellten Anforderungen umgesetzt werden. Es ist dann auch möglich, 
das bestehende Restrisiko zu akzeptieren. Dazu sollte bedacht werden, welche Bausteine 
in der Modellierung Anwendung fanden und ob die dort beschriebenen Maßnahmen ausreichend 
für das Zielobjekt sind. Schließlich soll diese Auflistung nicht (nur) dazu dienen, in 
einem Audit zu bestehen, sondern aktiv die Sicherheit des Informationsverbundes erhöhen. 
Insbesondere dadurch, dass einige Anforderungen der modellierten Bausteine nicht angewendet 
werden, da sie dem Primärziel des Clusters, dem performanten Rechnen, im Weg stehen, muss 
das Risiko ganz genau verstanden und abgeschätzt werden. So ist die Gefährdung 
\textit{G0.19 Offenlegung schützenswerter Informationen} zwar grundsätzlich durch den 
Baustein \textit{APP.6} abgedeckt, jedoch können Nutzende des Clusters üblicherweise 
beliebig auf die Login-Knoten zugreifen. Es muss also zusätzlich gewährleistet werden, 
dass für Projekte mit höherem Schutzbedarf eine entsprechende Konfiguration möglich ist. 
Ferner könnte die strikte Umsetzung der Maßnahmen auch Auswirkungen auf die Arbeitsweise 
haben. Forschungsanwendungen sind selten zertifiziert, zum Teil wird Software selber 
modifiziert und kompiliert. Durch die beschriebenen Maßnahmen des Bausteins müssen 
großflächig bestehende Prozesse angepasst werden. Dies erfordert viel Arbeit und sorgt 
nicht unbedingt für eine Verbesserung der Sicherheit, da eine Prozessänderung nicht 
gleichzeitig bedeutet, dass dieser Prozess auch sinnvoll angewandt wird. Gerade für diese Situation 
bietet der Grundschutz aber eine Flexibilität an. Der Zugriff auf allgemeine Dateien, die 
nicht relevant für das eigene Forschungsprojekt sind, kann durch die Konfiguration im
Betriebssystem eingestellt werden. Vor der Ausführung im Cluster wird Software auf die 
Stabilität und Korrektheit geprüft. Da Programme üblicherweise mehrere Stunden oder sogar 
Tage und Wochen laufen, ist es nicht möglich, sie zwischendurch zu unterbrechen und 
anzupassen (zu diesem Thema wird aber geforscht). Im gleichen Schritt könnte 
Software aber auch auf Sicherheit geprüft werden. Dies erfordert wiederum Aufwand und 
Konzeption im Clustermanagement. 
