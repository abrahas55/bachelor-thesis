# Timeline

Geplante Timeline für die restliche Arbeit 

| Datum      | Event |
|------------|-------|
| 2023-06-04 | Restrukturierung des bestehenden Textes |
| 2023-06-07 | Risikoanalyse draft, mindestens Grundkonzept und für ein Objekt |
| - | done |
| 2023-06-11 | Risikoanalyse konzeptionell fertig |
| 2023-06-14 | Grundfazit formulieren und Folien für Wissenstransfer konzipieren |
| 2023-06-15 | Besprechung Grundfazit, Risikoanalyse, Folien | 
| 2023-06-18 | Risikoanalyse fertig schreiben, Fazit anfangen, Folien feedbacken |
| 2023-06-22 | Arbeit in erster Version fertig, für Feedback einsenden | 
| 2023-07-07 | Wissenstransfer | 
| 2023-07-?? | Abgabe |
| 2023-07-?? | Abschlusspräsentation | 

# structure

Die Arbeit soll mit einem klar erkennbaren, roten Faden geführt werden. 

## Introduction

Dazu soll zunächst ins Thema eingeführt werden. Die Motivation, warum das ein
relevantes Thema ist und welche Probleme es eigentlich zu lösen gibt. 

## fundamentals

Was sind eigentlich Supercomputer, bzw. HPC-Cluster ? Dieses Kapitel soll den
Lesenden einen Einblick in das Thema HPC geben, damit die folgenden Abschnitte
trotzdem verständlich sind und einigermaßen eingeordnet werden können. 

### related work 

(*Dieser Part kann nahtlos übergehen, ist ja im Prinzip auch eine Einführung ins
Thema. Der Inhalt sollte eingewoben werden in die Einführung in das Thema*)
Passend dazu schauen wir uns an, was denn die Forschung zu dem Thema sagt,
welche Probleme sie dort sehen, welche Lösungsansätze getestet wurden und
funktionieren und welche nicht. 

Wir werden sehen, dass die Frage der Sicherheit zwar wahrgenommen wird, es aber kein
einheitliches Vorgehen gibt, wie sie sichergestellt werden kann. Dadurch ergibt
sich bei der Anwendung ein großes Problem, nicht nur in der Umsetzung (da das
alles erst geplant werden muss und eventuelle Probleme nicht betrachtet werden,
die eigentlich woanders schon gelöst wurden), sondern auch in der
Preis- und Aufwandsbetrachtung. Wir untersuchen den Grundschutz des BSI als
Werkzeug für diese Aufgabe. 

## Grundschutz / Methodik

Dazu kurze Erklärung, was Grundschutz eigentlich ist und wie die Methodik
aussieht. Insbesondere erwarten wir eher mittelmäßige Ergebnisse aus der
Modellierung und erhoffen uns, dass die strukturierte Risikoanalyse sehr
hilfreich ist. 

Insgesamt soll hier auch erläutert werden, welcher Maßstab angesetzt wird. Im
Grund geht es um die Frage, ob die Anwendung sinnvoll ist, d.h. ob der
Grundschutz dieses Werkzeug sein kann oder nicht. Ferner ist auch spannend, wie
der Aufwand war und ob dieser angemessen ist. 

## Betrachteter Informationsverbund 

Als System betrachten wir eine abstrahierte Version des HPC-Cluster Curta an der
Freien Universität Berlin. Einerseits habe ich Personen im näheren Umfeld, die
mit dem System arbeiten und forschen, auf der anderen Seite erlaubt die
Abstraktion, spezifische Details auszulassen und eine gewisse
Allgemeingültigkeit zu wahren. 

### Strukturanalyse

Zunächst beschreiben wir also den betrachteten Informationsverbund 

### Schutzbedarf

und definieren einen Schutzbedarf für die Prozesse und Anwendungen 

### Modellierung 

Anwendung der Bausteine, dabei Begründung, warum welche Bausteine nicht
betrachtet werden und auch welche Bausteine potentiell betrachtet werden müssen,
aber nicht unbedingt im Rahmen des Grundschutzes für das Cluster, da dieses
oftmals in einer höhergestuften Instanz (in diesem Fall die ZEDAT der FU)
untergeordnet ist. 

#### Erkenntnis 

welche Eigenschaften sind nicht abgedeckt, wofür müssen wir deshalb noch eine
Risikoanalyse heranziehen und warum? Lohnt sich eventuell ein Baustein? 
Ferner muss eine Risikoanalyse sowieso gestaltet werden für jene Systeme, die in
der Schutzbedarfsanalyse als hoch oder sehr hoch eingestuft wurden. 

### Risikoanalyse

Durchführung Risikoanalyse

## Fazit 

Fragen aus den obigen Teilen zusammenfügen, war das Ergebnis anwendbar? 

Sind vllt nur Teile des Grundschutzes anzuwenden? Können die Probleme durch ein
Grundschutzprofil gelöst werden? Was muss dafür noch getan werden. 

