% !TeX encoding = UTF-8
\section{Grundlagen}

% Was sind eigentlich Cluster, erklärt für Leute, die Cluster-Computing noch nicht gehört haben
Zunächst sollten wir den Begriff eines Clusters klären. Dabei gibt es neben den 
High-Perfomance-Computing (HPC) Clustern in dieser Arbeit auch High-Availability 
(HA) Cluster, welche in erster Linie über Redundanz die Verfügbarkeit eines 
Dienstes ermöglichen. Knoten, also die einzelnen Rechensysteme eines Cluster, 
können dabei selbst vollständige Computer sein oder nur aus wenigen, für die 
jeweilige Aufgabe notwendigen, Hardware-Teilen bestehen. In einem HA-Cluster 
kann jeder Knoten alleine die Aufgabe erfüllen, in einem HPC-System sind sie 
aber als Teil des Ganzen zu sehen. Historisch gesehen sind Cluster die logische
Folge aus immer billigeren, sogenannten off-the-shelf Computern (man denke an den
IBM PC). Anstatt wie bisher eine große Recheneinheit zu bauen, fing man an 
kleinere Systeme zusammenzusetzen und gemeinsam rechnen zu lassen. 

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.8\textwidth]{images/architecture.png}
    \caption{Eine mögliche Architektur für eine Cluster-Umgebung~\cite{brückner_2021}}
    \label{fig:architecture}
\end{figure}

Eine beispielhafte Clusterumgebung ist in Abbildung~\ref{fig:architecture} zu sehen. 
Im Grunde ist das Gesamtkonstrukt sehr ähnlich zu dem wohlbekannten von-Neumann
Rechnermodell. Es gibt die Rechenknoten (in der Abbildung \textit{Compute Nodes}) 
als unsere CPU, die Login und Export Nodes als I/O und schließlich das Speichersystem. 
Ebenfalls wichtig für ein Cluster ist die Netztopologie, also die Struktur, 
die die Knoten verbindet. Es gibt verschiedene Varianten und je nach Anwendung
haben Topologien Vor- und Nachteile. 

HPC-Cluster (im Folgenden werde ich das HPC weglassen) werden nicht nur im
akademischen Umfeld angewandt, sondern auch im unternehmerischen Kontext. 
Die Anwendungen sind dabei verschiedenster Natur und in vielen Branchen 
vorzufinden. So kann durch die Rechenleistung des Clusters die Aerodynamik 
eines Autos oder Flugzeuges berechnet werden. Komplizierte Materialien und 
Polymere können in Simulationen getestet werden, anstatt sie kostspielig im 
Labor herzustellen. Nicht ohne Grund haben viele internationale Entitäten 
\textit{High-Performance-Computing}-Programme~\cite{digitalstrategie-eu}~\cite{digitalstrategie-usa}.
Neben der Forschung mithilfe des Clusters findet natürlich auch Forschung an
Clustern statt. Wie der Name schon sagt, geht es bei diesen Systemen um
eine möglichst hohe Performanz, sodass dort natürlich auch der Fokus der 
Forschung liegt.~\cite{perf-optimizations}~\cite{mapping-optimizations}

Die Abbildung von oben stammt aus einer Analyse nach einer großen Angriffswelle
auf Clustersysteme in Europa Mitte 2020~\cite{brückner_2021}. Der Artikel zeigt 
einen interessanten Angriffsweg und wie \textit{lateral movement}, also die 
Bewegung eines Angreifers nach dem initialen Einstiegspunkt, im Cluster 
funktionieren kann. Der Autor gibt einen Einblick in die typischen 
Sicherheitsmaßnahmen einer Cluster-Umgebung und nennt Methoden wie die 
Mehr-Faktor-Authentifizierung, die gegen den beschriebenen Angriff geholfen 
hätten. Vor allem wird klar, dass auch HPC-Cluster Ziele von Angriffen sind und
Betreiber Maßnahmen für die Sicherheit treffen sollten. Auch sollte die Forschung
diese Aspekte berücksichtigen.

\subsection{Security im High-Performance-Computing}

% Was ist eigentlich Sicherheit und wie wird das üblicherweise gemessen? 

Dabei ist diese Erkenntnis keineswegs neu. Bereits 2004 bemerkten Yurcik et
al.~\cite{yurcik_2004} den in der Forschung und Anwendung eher üblichen Fokus 
auf die Performanz, wobei die Sicherheit des Systems stets eine untergeordnete 
Rolle einnahm. Die Autoren beschreiben die Sicherheit eines Clustersystems als stark 
unterschiedlich zur Sicherheit eines Büro-IT-Systems. Für die Gesamtsicherheit ist 
nicht nur die Sicherheit der einzelnen Knoten zu betrachten, sondern auch die 
Kommunikation dazwischen und nach außen. Sie schlagen vor, die Sicherheit von 
solch vernetzten Systemen als ganzheitliches Problem zu betrachten. Die vorgeschlagenen 
Lösungen wurden am \textit{National Center for Supercomputing Applications} in 
Illinois entwickelt und umgesetzt. Als Hauptergebnis liefern die Autoren ein 
Tool, welches Prozessmonitoring, Portscanner und Netzwerkverkehr-Analyse vereint. 
Interessanterweise sind diese Betrachtungen auch in üblichen Systemen durchaus 
relevant, jedoch ergeben sich aufgrund der Eigenheit von Clustersystemen nicht 
notwendigerweise einheitliche Maßstäbe zur Bewertung. 

Auch 13 Jahre nach der Arbeit von \textit{Yurcik et al.~} betonen Forschende 
immernoch den Unterschied von Clustersicherheit zu traditionellen 
Informationssystemen. Peisert~\cite{peisert_2017} betrachtet in seinem Artikel 
konkrete Methoden und deren Vor- bzw.\@ Nachteile für HPC-Systeme. So sei der 
klassische Ansatz einer Firewall mit \textit{deep packet inspection} für reguläre 
IT-Infrastrukturen angemessen, jedoch behindert die mögliche Latenz den enormen 
Datendurchsatz im Netzwerk, der für ein HPC-System notwendig ist. Als sinnvolle 
Methode wird die \textit{science-DMZ}~\cite{science_dmz} vorgestellt. Dabei wird 
u.~A.\@ der wissenschaftliche Datenverkehr (also auch das Rechnen auf den 
Knoten) in eine eigene Zone verschoben. Somit entsteht eine leichter zu 
beobachtende Dateninfrastruktur, da die Daten nur noch über eine Schnittstelle 
in den Forschungsbereich gelangen. Der Autor betont den Mehrwert der 
Komplexitätsreduktion des Systems für die Sicherheit, eine Maßnahme, die auch 
in regulären Infrastrukturen Anwendung findet. 

Je nach Einsatzzweck sind unterschiedliche Sicherheitsannahmen für IT-Systeme
zu treffen. Für die Forschung mit offenen Daten kann es irrelevant sein, wer 
diese Daten während der Verarbeitung potentiell einlesen kann. Werden jedoch 
personenbezogene Daten verwendet, müssen diese auch in angemessener Form 
geschützt werden. Wie das aussehen könnte, beschreiben Luo et 
al.~\cite{luo_2019}. Die Autoren erläutern die zum Zeitpunkt der 
Veröffentlichung bekannten Angriffs- und Verteidungswege in HPC-Systemen. Der 
Fokus liegt dabei auf Log-basierten Verfahren, die zunächst System- und 
Anwendungslogs sammeln, um mit den gesammelten Daten ein IDS (intrusion 
detection system) zu betreiben. Dies ist ein schönes Beispiel für theoretische 
Arbeiten zu dem Thema, denn die Autoren zeigen zwar die potentiellen Möglichkeiten 
der Verteidigung, am Ende bleibt jedoch die Frage offen, wie ein solches System 
denn überhaupt gebaut werden kann. Insbesondere haben Universitätscluster einen speziellen 
Hintergrund, da sie oftmals Infrastruktur für internationale Forschungsgruppen
bereitstellen. Sie profitieren dadurch von staatlicher Förderung, wodurch sich 
wiederum neue Anforderungen, auch aus der Perspektive der Informationssicherheit,
an die Systeme ergeben. Es ist auch üblich, dass mehrere Institutionen gemeinsam
an der Infrastruktur arbeiten. Eine solche Möglichkeit des wissenschaftlichen 
Zusammenarbeitens im Bereich HPC ist das Projekt DEISA. In seiner Habilitation 
betrachtet Hommel~\cite{hommel_2012} in einem Szenario das Projekt und leitet 
die notwendigen funktionalen Anforderungen an ein mögliches Security-Framework 
ab. Insbesondere wird hervorgehoben, dass die beteiligten Rechenzentren 
(sogenannte DEISA-sites) jeweils selbst für die Sicherheit im eigenen Bereich 
verantwortlich sind. Dadurch ergibt sich eine gewisse Autonomie, jedoch werden 
auch Prozesse und Erfahrungen von mehreren Institutionen einzeln gemacht, 
anstatt einen einheitlichen Ansatz zu wählen und doppelte Arbeit zu sparen. 

Auch wenn es einige Anstrengungen dahingehend gibt, fehlt doch ein einheitliches
Vorgehen zur Absicherung einer Clusterumgebung. Dazu sollte auch noch die Frage 
der Sicherheit angesprochen werden. Mittlerweile gibt es eine recht einheitliche
Meinung dazu, wie Sicherheit in einem System zu bewerten ist. Dabei wird das 
Tripel \textit{Vertraulichkeit, Integrität und Verfügbarkeit} (im engl.:\@ CIA: 
\textit{Confidentiality, Integrity, Availability}) als Basis der 
Betrachtung genommen. Vertraulichkeit beschreibt dabei die Eigenschaft eines 
Datums, nicht von ungewollten Dritten lesbar zu sein. Wenn für ein Datum eine 
hohe Integrität angenommen wird, dann meint das die Unveränderbarkeit dieses
Datums und Verfügbarkeit sollte selbsterklärend sein. Im Rahmen einer 
Sicherheitsuntersuchung wird oftmals die Relevanz dieser sogenannten Grundwerte
eingeschätzt. Ein Dienst, der personenbezogene Daten verarbeitet sollte 
stärker auf die Vertraulichkeit achten als eine einfache Webseite. Dagegen 
kann die Webseite aber extrem relevant sein und sollte somit eine höhere Verfügbarkeit
vorweisen als der Dienst. Je nach Bedarf können auch weitere Kriterien wie 
Verbindlichkeit und Zurechenbarkeit betrachtet werden. 

Ich möchte in dieser Arbeit einen Ansatz zum standardisierten Sichern eines 
HPC-Clusters vorschlagen. Dazu betrachte ich zunächst die existierenden und 
verbreiteten Standards, insbesondere aber den IT-Grundschutz des BSI.

\subsection{Standardisierungen und Zertifizierungen}

% IT-Grundschutz, BSI, 
Der IT-Grundschutz, im Folgenden nur Grundschutz genannt, ist mittlerweile ein 
weit verbreitetes Werkzeug in Deutschland. Federführend entwickelt durch das 
Bundesamt für Sicherheit in der Informationstechnik (BSI) ist es der Standard
zur Konzeption und Erstellung eines Sicherheitskonzeptes (SiKo).

In der aktuellen Version besteht der Grundschutz aus mehreren Dokumenten, die 
symbiotisch anzuwenden sind. Im \textit{BSI Standard 200-1}\cite{bsi-200-1} 
wird beschrieben, welche Anforderungen ein Informationssicherheitsmanagementsystem 
(kurz: ISMS) erfüllen muss. Der Name erschließt sich aus der Grunddefinition der 
\textit{Informationssicherheit} im Gegensatz zum früher angewandten Begriff 
\textit{IT-Sicherheit}. Damit soll hervorgehoben werden, dass nunmehr nicht 
nur IT-Systeme, sondern die Sicherheit aller \textit{Informationen} im 
(Informations-)Verbund betrachtet werden sollte. Als Verbund bezeichnen wir 
alle betrachteten Objekte. Das kann manchmal die gesamte Institution sein, 
es ist aber auch möglich nur einen Teil zu betrachten. Ein ISMS umfasst also 
neben der Frage der Sicherheit dieser Systeme auch die umgebenden Prozesse 
z.B.\@ zur Vorfallbehandlung oder dem Business Continuity Management (BCM). 
Näheres können interessierte Lesende selbst im Dokument nachschlagen, der 
kurze Überblick soll uns an dieser Stelle genügen.

Wie man nun dabei vorgehen sollte wird im nächsten \textit{BSI Standard 200-2} 
erläutert. Dabei kann man grundsätzlich unterscheiden zwischen einer Basis-, 
Standard- oder Kernabsicherung. In erster Linie unterscheidet das BSI dabei nach
den Voraussetzungen bezüglich dem bereits existierenden Sicherheitsniveau (wird
in der Institution bereits mit dem Grundschutz gearbeitet?) und der Übersicht 
über die vorhandenen Geschäftsprozesse. Darunter versteht man die Prozesse der 
Institution, die für die Erreichung der Geschäftsziele notwendig sind. So ist 
die Basis-Absicherung als breite Erst-Absicherung für den Einstieg in den 
IT-Grundschutz gedacht, die Kern-Absicherung überprüft nur die wichtigsten Komponenten 
und die Standard-Absicherung umfasst die gesamte Institution. Es sei aber 
darauf hingewiesen, dass erst die Standardabsicherung ein vollumfängliches ISMS 
als Ziel hat und deshalb auch das vom BSI empfohlene Verfahren darstellt. Dabei
kann nach erfolgreichem Abschluss einer Basis- oder Kern-Absicherung die 
Standard-Absicherung direkt gestartet werden.
 
Komplementär zu den vorgestellten Schritten veröffentlicht das BSI jedes Jahr 
ein neues \textit{IT-Grundschutz-Kompendium}. Für die vorliegende Arbeit wurde
die Version des Jahres 2023 zugrunde gelegt. Darin sind Bausteine für viele 
Anwendungen und Themen aufgeführt, die je nach Bedarf eingesetzt werden können. 
Zusätzlich sind für jeden Baustein konkrete Sicherheitsanforderungen definiert, 
aus denen sich direkt Handlungsempfehlungen herleiten lassen. 

Da der Grundschutz aber aus offensichtlichen Gründen nicht jede mögliche 
Systemkomponente, Anwendung oder Gefährdung abdecken kann, sieht der Grundschutz
eine Risikoanalyse vor. Dabei hilft das vorhergegangene, strukturierte Vorgehen, 
da somit nicht mehr alle Teile betrachtet werden müssen, sondern nur noch jene, 
bei denen ein erhöhtes Risiko festzustellen ist oder es keine angemessenen Bausteine
gibt. Das Vorgehen und die Einbettung in das Sicherheitskonzept wird im 
\textit{BSI Standard 200-3} beschrieben.

Abschließend zählt noch der \textit{BSI Standard 200-4} dazu. Dieser umfasst 
dabei das sogenannte \textit{Business Continuity Management (BCM)}, welches den
Wiederanlauf der Geschäftsprozesse nach einem Vorfall beinhaltet. 

In den veröffentlichten Standards bezieht das BSI auch Fachexperten z.B. aus der
Wirtschaft ein und aktualisiert die Inhalte entsprechend. Als Regierungsinstitution
unterstützt das Bundesamt Unternehmen und Behörden in der Organisation und Umsetzung
des Standards. Einen besonderen Fokus haben dabei kritische Infrastrukturen (KRITIS), 
deren Ausfall offensichtliche Probleme zur Folge hätte. Sich selbst beschreibt 
das BSI als \enquote{Cyber-Sicherheitsbehörde des Bundes und Gestalter einer 
sicheren Digitalisierung in Deutschland}. Zusätzlich zu der Veröffentlichung in 
deutscher Sprache werden die Dokumente auch in englischer Sprache zur Verfügung 
gestellt. Neben einem Einsatz im unternehmerischen und behördlichen Kontext ist 
der IT-Grundschutz bereits auch Forschungsgegenstand. Grenz verwendet den 
Grundschutz in seiner Dissertation~\cite{grenz_2012} zur Konzeption einer 
Sicherheitsarchitektur für medizinische Forschungsnetze. Er ist allgemein auf 
IT-Strukturen anwendbar und ermöglicht eine vergleichbare Einschätzung für den 
Basisschutz. Insbesondere hebt der Autor den Maßnahmenkatalog hervor, welcher in 
dieser Form in anderen Standards nicht zu finden ist. 

Den Grundschutz kann man natürlich einfach so aus Freude an der Sache (und
Sicherheit) durchführen, oftmals wird aber dazu noch eine Zertifizierung nach 
ISO\footnote{International Organization for Standardization \url{iso.org}} 27001 
angestrebt. Dieser internationale Standard beschreibt die Anforderungen an ein 
ISMS und ist weltweit anerkannt. Da der IT-Grundschutz auf dem ISO-Standard aufbaut,
kann eine Zertifizierung nach einer Standard- (und in einigen Fällen auch nach 
einer Kern-)Absicherung stattfinden.
